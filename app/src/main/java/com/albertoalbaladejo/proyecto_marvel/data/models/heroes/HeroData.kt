package com.albertoalbaladejo.proyecto_marvel.data.models.heroes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HeroData(val results: List<HeroResult>) : Parcelable