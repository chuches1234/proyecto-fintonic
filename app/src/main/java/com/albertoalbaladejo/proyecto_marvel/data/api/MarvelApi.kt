package com.albertoalbaladejo.proyecto_marvel.data.api

import com.albertoalbaladejo.proyecto_marvel.data.models.comic.ComicResponse
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResponse
import com.albertoalbaladejo.proyecto_marvel.data.models.series.SeriesResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MarvelApi {

    @GET("characters")
    suspend fun getAllHeros(
        @Query("offset") offset: Int? = 0,
        @Query("limit") limit: Int? = 20
    ): HeroResponse

    @GET("characters")
    suspend fun searchHero(
        @Query("nameStartsWith") query: String,
        @Query("offset") offset: Int? = 0,
        @Query("limit") limit: Int? = 20
    ): HeroResponse

    @GET("characters/{characterId}/comics")
    suspend fun getHeroComics(
        @Path("characterId") characterId: String,
        @Query("offset") offset: Int? = 0,
        @Query("limit") limit: Int? = 20
    ): ComicResponse

    @GET("characters/{characterId}/series")
    suspend fun getHeroSeries(
        @Path("characterId") characterId: String,
        @Query("offset") offset: Int? = 0,
        @Query("limit") limit: Int? = 20
    ): SeriesResponse

}