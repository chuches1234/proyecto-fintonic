package com.albertoalbaladejo.proyecto_marvel.data.models.heroes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HeroResponse(val data: HeroData) : Parcelable