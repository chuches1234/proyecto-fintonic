package com.albertoalbaladejo.proyecto_marvel.data.models.series

data class SeriesData(val results: List<SeriesResult>)