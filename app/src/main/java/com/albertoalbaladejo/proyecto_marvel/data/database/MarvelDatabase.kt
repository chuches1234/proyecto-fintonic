package com.albertoalbaladejo.proyecto_marvel.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import com.albertoalbaladejo.proyecto_marvel.utils.Constants

@Database(entities = [HeroResult::class], version = Constants.DbConstant.DB_VERSION)
abstract class MarvelDatabase : RoomDatabase() {

    abstract fun getHeroDao(): HeroDao

}