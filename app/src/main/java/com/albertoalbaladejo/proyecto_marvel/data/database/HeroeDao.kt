package com.albertoalbaladejo.proyecto_marvel.data.database

import androidx.room.*
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import kotlinx.coroutines.flow.Flow

@Dao
interface HeroDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(hero: HeroResult)

    @Delete
    suspend fun delete(hero: HeroResult)

    @Query("SELECT * FROM heroes_table")
    fun getFavouriteHeroes(): Flow<List<HeroResult>>

}