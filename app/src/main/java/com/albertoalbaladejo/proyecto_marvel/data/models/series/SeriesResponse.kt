package com.albertoalbaladejo.proyecto_marvel.data.models.series

data class SeriesResponse(val data: SeriesData)