package com.albertoalbaladejo.proyecto_marvel.data.models.comic

data class ComicResponse(val data: ComicData)