package com.albertoalbaladejo.proyecto_marvel.data.api.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.albertoalbaladejo.proyecto_marvel.data.api.MarvelApi
import com.albertoalbaladejo.proyecto_marvel.data.database.HeroDao
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import com.albertoalbaladejo.proyecto_marvel.ui.allheroes.HeroPagingSource
import com.albertoalbaladejo.proyecto_marvel.ui.herocomics.ComicPagingSource
import com.albertoalbaladejo.proyecto_marvel.ui.heroeseries.SeriesPagingSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MarvelRepository @Inject constructor(
    private val marvelApi: MarvelApi,
    private val heroDao: HeroDao
) {

    fun searchHero(query: String) = Pager(
        config = PagingConfig(pageSize = 20, maxSize = 100, enablePlaceholders = false),
        pagingSourceFactory = { HeroPagingSource(marvelApi, query) }
    ).flow

    fun getHeroComics(heroId: String) = Pager(
        config = PagingConfig(pageSize = 10, maxSize = 50, enablePlaceholders = false),
        pagingSourceFactory = { ComicPagingSource(marvelApi, heroId) }
    ).flow

    fun getHeroSeries(heroId: String) = Pager(
        config = PagingConfig(pageSize = 10, maxSize = 50, enablePlaceholders = false),
        pagingSourceFactory = { SeriesPagingSource(marvelApi, heroId) }
    ).flow

    suspend fun addHeroToFavourite(heroResult: HeroResult) =
        heroDao.insert(heroResult)

    suspend fun removeHeroFromFavourite(heroResult: HeroResult) =
        heroDao.delete(heroResult)

    fun getFavouriteHeros() =
        heroDao.getFavouriteHeroes()

}