package com.albertoalbaladejo.proyecto_marvel.data.models.comic

data class ComicData(val results: List<ComicResult>)