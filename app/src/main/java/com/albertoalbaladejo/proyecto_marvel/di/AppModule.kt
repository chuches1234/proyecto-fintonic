package com.albertoalbaladejo.proyecto_marvel.di

import android.content.Context
import androidx.room.Room
import com.albertoalbaladejo.proyecto_marvel.data.api.MarvelApi

import com.albertoalbaladejo.proyecto_marvel.data.database.HeroDao
import com.albertoalbaladejo.proyecto_marvel.data.database.MarvelDatabase
import com.albertoalbaladejo.proyecto_marvel.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url

            val url = originalHttpUrl.newBuilder()
                .addQueryParameter(Constants.AppModuleKey.API_KEY, Constants.PUBLIC_KEY)
                .addQueryParameter(Constants.AppModuleKey.TIMESTAMP, Constants.timeStamp)
                .addQueryParameter(Constants.AppModuleKey.HASH_KEY, Constants.hash())
                .build()

            chain.proceed(original.newBuilder().url(url).build())
        }
        return httpClient.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(httpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .build()

    @Provides
    @Singleton
    fun provideMarvelApi(retrofit: Retrofit): MarvelApi = retrofit.create(MarvelApi::class.java)

    @Provides
    @Singleton
    fun provideMarvelDatabase(@ApplicationContext context: Context): MarvelDatabase =
        Room.databaseBuilder(context, MarvelDatabase::class.java, Constants.DbConstant.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideHeroDao(marvelDatabase: MarvelDatabase): HeroDao =
        marvelDatabase.getHeroDao()

}