package com.albertoalbaladejo.proyecto_marvel.ui.favouritehero

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import com.albertoalbaladejo.proyecto_marvel.ui.allheroes.HeroClickListener
import com.albertoalbaladejo.proyecto_marvel.utils.Mapper.HERO_MAPPER
import com.albertoalbaladejocortes.proyecto_marvel.R
import com.albertoalbaladejocortes.proyecto_marvel.databinding.ItemHeroBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

class FavouriteHeroAdapter(private val listener: HeroClickListener) :
    ListAdapter<HeroResult, FavouriteHeroAdapter.FavouriteHeroViewHolder>(
        HERO_MAPPER
    ) {

    inner class FavouriteHeroViewHolder(private val binding: ItemHeroBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bindHero(hero: HeroResult) {
            binding.apply {
                Glide.with(itemView)
                    .load(hero.thumbnail.path + "." + hero.thumbnail.extension)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            heroProgressbar.isVisible = false
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            heroProgressbar.isVisible = false
                            return false
                        }
                    })
                    .centerCrop().into(heroImage)
                heroName.text = hero.name
                val description = hero.description
                if (description == "") {
                    heroDescription.text = "No Description"
                } else {
                    heroDescription.text = hero.description
                }
                heroComics.text = "Comics: ${hero.comics.available}"
                heroSeries.text = "Series: ${hero.series.available}"
                likeButton.setImageResource(R.drawable.ic_favourite_red)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FavouriteHeroViewHolder {
        val binding =
            ItemHeroBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val favouriteHeroViewHolder = FavouriteHeroViewHolder(binding)

        with(binding) {
            heroCardView.animation =
                AnimationUtils.loadAnimation(binding.heroCardView.context, R.anim.translate)
            heroCardView.setOnClickListener {
                listener.onClick(getItem(favouriteHeroViewHolder.absoluteAdapterPosition))
            }
            likeButton.setOnClickListener {
                listener.removeFromFavourite(getItem(favouriteHeroViewHolder.absoluteAdapterPosition))
            }
        }

        return favouriteHeroViewHolder
    }

    override fun onBindViewHolder(holder: FavouriteHeroViewHolder, position: Int) {
        val hero = getItem(position)
        holder.bindHero(hero)
    }
}