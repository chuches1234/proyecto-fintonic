package com.albertoalbaladejo.proyecto_marvel.ui.favouritehero

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import com.albertoalbaladejocortes.proyecto_marvel.databinding.FragmentFavouriteHeroesBinding
import com.albertoalbaladejo.proyecto_marvel.ui.allheroes.HeroClickListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavouriteHeroFragment : Fragment(),
    HeroClickListener {

    private lateinit var binding: FragmentFavouriteHeroesBinding
    private val favouriteHeroViewModel: FavouriteHeroViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavouriteHeroesBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val favouriteHeroAdapter = FavouriteHeroAdapter(this)

        binding.recyclerview.apply {
            adapter = favouriteHeroAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        favouriteHeroViewModel.getFavouriteHeros().observe(viewLifecycleOwner) {
            favouriteHeroAdapter.submitList(it)
        }

    }

    override fun onClick(hero: HeroResult) {
        val action =
            FavouriteHeroFragmentDirections.actionFavouriteHeroFragmentToHeroDetailFragment(
                hero
            )
        findNavController().navigate(action)
    }

    override fun addToFavourite(hero: HeroResult) {
        favouriteHeroViewModel.addToFavourite(hero)
    }

    override fun removeFromFavourite(hero: HeroResult) {
        favouriteHeroViewModel.removeFromFavourite(hero)
    }
}