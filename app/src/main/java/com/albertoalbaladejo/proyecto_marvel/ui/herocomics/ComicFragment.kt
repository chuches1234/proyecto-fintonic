package com.albertoalbaladejo.proyecto_marvel.ui.herocomics

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.albertoalbaladejo.proyecto_marvel.data.models.comic.ComicResult
import com.albertoalbaladejo.proyecto_marvel.ui.allheroes.MarvelLoadStateAdapter
import com.albertoalbaladejo.proyecto_marvel.ui.herodetail.HeroDetailFragmentDirections
import com.albertoalbaladejocortes.proyecto_marvel.R
import com.albertoalbaladejocortes.proyecto_marvel.databinding.FragmentComicBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ComicFragment(private val heroId: String) :
    Fragment(R.layout.fragment_comic), ComicClickListener {

    private var _binding: FragmentComicBinding? = null
    private val binding get() = _binding!!
    private val comicViewModel by viewModels<ComicViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentComicBinding.bind(view)

        val comicAdapter = ComicRecyclerViewAdapter(this)

        comicAdapter.addLoadStateListener { loadState ->
            binding.apply {
                comicProgressBar.isVisible = loadState.source.refresh is LoadState.Loading
                comicRecyclerView.isVisible = loadState.source.refresh is LoadState.NotLoading
                comicRetryButton.isVisible = loadState.source.refresh is LoadState.Error
                comicNoConnection.isVisible = loadState.source.refresh is LoadState.Error
                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached && comicAdapter.itemCount < 1
                ) {
                    comicRecyclerView.isVisible = false
                    noResultFoundTextView.isVisible = true
                } else {
                    noResultFoundTextView.isVisible = false
                }
            }
        }

        binding.comicRecyclerView.apply {
            adapter = comicAdapter.withLoadStateHeaderAndFooter(
                header = MarvelLoadStateAdapter { comicAdapter.retry() },
                footer = MarvelLoadStateAdapter { comicAdapter.retry() }
            )
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }

        binding.apply {
            comicRetryButton.setOnClickListener {
                comicAdapter.retry()
            }
        }

        comicViewModel.getHeroComics(heroId).observe(viewLifecycleOwner) {
            comicAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

    }

    override fun showComicDetail(comic: ComicResult) {
        binding.apply {
            val action =
                HeroDetailFragmentDirections.actionHeroDetailFragmentToInfoHeroFragment(
                    comic.thumbnail.path,
                    comic.thumbnail.extension,
                    comic.title,
                    comic.description
                )
            findNavController().navigate(action)
        }
    }

    

}