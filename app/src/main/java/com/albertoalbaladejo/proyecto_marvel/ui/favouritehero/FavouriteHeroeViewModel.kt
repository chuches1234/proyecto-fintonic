package com.albertoalbaladejo.proyecto_marvel.ui.favouritehero

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.albertoalbaladejo.proyecto_marvel.data.api.repository.MarvelRepository
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavouriteHeroViewModel @Inject constructor(private val marvelRepository: MarvelRepository) :
    ViewModel() {

    fun addToFavourite(heroResult: HeroResult) =
        viewModelScope.launch {
            marvelRepository.addHeroToFavourite(heroResult)
        }

    fun removeFromFavourite(heroResult: HeroResult) =
        viewModelScope.launch {
            marvelRepository.removeHeroFromFavourite(heroResult)
        }

    fun getFavouriteHeros() =
        marvelRepository.getFavouriteHeros().asLiveData()

}