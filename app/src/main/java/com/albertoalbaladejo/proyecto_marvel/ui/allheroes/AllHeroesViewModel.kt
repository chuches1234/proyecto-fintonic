package com.albertoalbaladejo.proyecto_marvel.ui.allheroes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.albertoalbaladejo.proyecto_marvel.data.api.repository.MarvelRepository
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AllHeroesViewModel @Inject constructor(private val marvelRepository: MarvelRepository) :
    ViewModel() {

    val searchQuery = MutableStateFlow("")

    @OptIn(ExperimentalCoroutinesApi::class)
    val searchResult = searchQuery.flatMapLatest { query ->
        marvelRepository.searchHero(query).cachedIn(viewModelScope)
    }.asLiveData()

    fun addToFavourite(heroResult: HeroResult) =
        viewModelScope.launch {
            marvelRepository.addHeroToFavourite(heroResult)
        }

    fun removeFromFavourite(heroResult: HeroResult) =
        viewModelScope.launch {
            marvelRepository.removeHeroFromFavourite(heroResult)
        }

    fun getFavouriteHeroes() =
        marvelRepository.getFavouriteHeros().asLiveData()
}