package com.albertoalbaladejo.proyecto_marvel.ui.herocomics

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.albertoalbaladejo.proyecto_marvel.data.models.comic.ComicResult
import com.albertoalbaladejo.proyecto_marvel.utils.Mapper.HERO_COMICS_MAPPER
import com.albertoalbaladejocortes.proyecto_marvel.R
import com.albertoalbaladejocortes.proyecto_marvel.databinding.ItemComicBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

class ComicRecyclerViewAdapter(private val listener: ComicClickListener) :
    PagingDataAdapter<ComicResult, ComicRecyclerViewAdapter.ComicViewHolder>(HERO_COMICS_MAPPER) {

    inner class ComicViewHolder(private val binding: ItemComicBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comic: ComicResult) {
            binding.apply {
                Glide.with(itemView)
                    .load(comic.thumbnail.path + "." + comic.thumbnail.extension)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            comicProgressbar.isVisible = false
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            comicProgressbar.isVisible = false
                            return false
                        }
                    })
                    .centerCrop().into(comicImage)
                comicName.text = comic.title
                comicDescription.text = comic.description
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicViewHolder {
        val binding = ItemComicBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val comicViewHolder = ComicViewHolder(binding)
        binding.comicCardView.animation =
            AnimationUtils.loadAnimation(binding.comicCardView.context, R.anim.translate)
        binding.comicCardView.setOnClickListener {
            listener.showComicDetail(getItem(comicViewHolder.absoluteAdapterPosition)!!)
        }
        return comicViewHolder
    }

    override fun onBindViewHolder(holder: ComicViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }
}

interface ComicClickListener {
    fun showComicDetail(comic: ComicResult)
}