package com.albertoalbaladejo.proyecto_marvel.ui.heroseries

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.albertoalbaladejocortes.proyecto_marvel.R
import com.albertoalbaladejo.proyecto_marvel.data.models.series.SeriesResult
import com.albertoalbaladejocortes.proyecto_marvel.databinding.FragmentSeriesBinding
import com.albertoalbaladejo.proyecto_marvel.ui.allheroes.MarvelLoadStateAdapter
import com.albertoalbaladejo.proyecto_marvel.ui.herodetail.HeroDetailFragmentDirections
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SeriesFragment(private val heroId: String) :
    Fragment(R.layout.fragment_series), SeriesClickListener {

    private var _binding: FragmentSeriesBinding? = null
    private val binding get() = _binding!!
    private val seriesViewModel by viewModels<SeriesViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSeriesBinding.bind(view)

        val seriesAdapter = SeriesRecyclerViewAdapter(this)

        seriesAdapter.addLoadStateListener { loadState ->
            binding.apply {
                seriesProgressBar.isVisible = loadState.source.refresh is LoadState.Loading
                seriesRecyclerView.isVisible = loadState.source.refresh is LoadState.NotLoading
                seriesRetryButton.isVisible = loadState.source.refresh is LoadState.Error
                seriesNoConnection.isVisible = loadState.source.refresh is LoadState.Error
                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached && seriesAdapter.itemCount < 1
                ) {
                    seriesRecyclerView.isVisible = false
                    noResultFoundTextView.isVisible = true
                } else {
                    noResultFoundTextView.isVisible = false
                }
            }
        }

        binding.seriesRecyclerView.apply {
            adapter = seriesAdapter.withLoadStateHeaderAndFooter(
                header = MarvelLoadStateAdapter { seriesAdapter.retry() },
                footer = MarvelLoadStateAdapter { seriesAdapter.retry() }
            )
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }

        binding.apply {
            seriesRetryButton.setOnClickListener {
                seriesAdapter.retry()
            }
        }

        seriesViewModel.getHeroSeries(heroId).observe(viewLifecycleOwner) {
            seriesAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

    }

    override fun showSeriesDetail(series: SeriesResult) {
        binding.apply {
            val action =
                HeroDetailFragmentDirections.actionHeroDetailFragmentToInfoHeroFragment(
                    series.thumbnail.path,
                    series.thumbnail.extension,
                    series.title,
                    series.description
                )
            findNavController().navigate(action)
        }
    }

    

}