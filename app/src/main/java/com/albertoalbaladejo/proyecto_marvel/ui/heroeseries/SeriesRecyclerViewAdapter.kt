package com.albertoalbaladejo.proyecto_marvel.ui.heroseries

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.albertoalbaladejocortes.proyecto_marvel.R
import com.albertoalbaladejo.proyecto_marvel.data.models.series.SeriesResult
import com.albertoalbaladejocortes.proyecto_marvel.databinding.ItemSeriesBinding
import com.albertoalbaladejo.proyecto_marvel.utils.Mapper.HERO_SERIES_MAPPER
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

class SeriesRecyclerViewAdapter(private val listener: SeriesClickListener) :
    PagingDataAdapter<SeriesResult, SeriesRecyclerViewAdapter.SeriesViewHolder>(
        HERO_SERIES_MAPPER
    ) {

    inner class SeriesViewHolder(private val binding: ItemSeriesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(series: SeriesResult) {
            binding.apply {
                Glide.with(itemView)
                    .load(series.thumbnail.path + "." + series.thumbnail.extension)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            seriesProgressbar.isVisible = false
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            seriesProgressbar.isVisible = false
                            return false
                        }
                    })
                    .centerCrop().into(seriesImage)
                seriesName.text = series.title
                seriesDescription.text = series.description
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeriesViewHolder {
        val binding = ItemSeriesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val seriesViewHolder = SeriesViewHolder(binding)
        binding.seriesCardView.animation =
            AnimationUtils.loadAnimation(binding.seriesCardView.context, R.anim.translate)
        binding.seriesCardView.setOnClickListener {
            listener.showSeriesDetail(getItem(seriesViewHolder.absoluteAdapterPosition)!!)
        }
        return seriesViewHolder
    }

    override fun onBindViewHolder(holder: SeriesViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }
}

interface SeriesClickListener {
    fun showSeriesDetail(series: SeriesResult)
}