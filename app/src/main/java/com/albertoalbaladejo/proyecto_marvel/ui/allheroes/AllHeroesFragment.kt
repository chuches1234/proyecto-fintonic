package com.albertoalbaladejo.proyecto_marvel.ui.allheroes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.albertoalbaladejocortes.proyecto_marvel.R
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import com.albertoalbaladejocortes.proyecto_marvel.databinding.FragmentAllHeroesBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AllHeroesFragment : Fragment(), HeroClickListener {

    private lateinit var binding: FragmentAllHeroesBinding

    private val allHeroViewModel: AllHeroesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAllHeroesBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initRecyclerView()
    }

    private fun initToolbar() {
        val mainActivity = requireActivity() as AppCompatActivity
        mainActivity.setSupportActionBar(binding.toolbar)
        setHasOptionsMenu(true)
    }

    private fun initRecyclerView() {
        val allHeroAdapter = AllHeroAdapter(this)

        allHeroAdapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                recyclerview.isVisible = loadState.source.refresh is LoadState.NotLoading
                allHeroRetryButton.isVisible = loadState.source.refresh is LoadState.Error
                allHeroNoConnection.isVisible = loadState.source.refresh is LoadState.Error

                if (loadState.source.refresh is LoadState.NotLoading &&
                    loadState.append.endOfPaginationReached && allHeroAdapter.itemCount < 1
                ) {
                    recyclerview.isVisible = false
                    noResultFoundTextView.isVisible = true
                } else {
                    noResultFoundTextView.isVisible = false
                }
            }
        }

        with(binding) {
            recyclerview.apply {
                layoutManager = GridLayoutManager(requireContext(), 2)
                adapter = allHeroAdapter.withLoadStateHeaderAndFooter(
                    header = MarvelLoadStateAdapter { allHeroAdapter.retry() },
                    footer = MarvelLoadStateAdapter { allHeroAdapter.retry() }
                )
                setHasFixedSize(true)
            }
            allHeroRetryButton.setOnClickListener {
                allHeroAdapter.retry()
            }
        }

        allHeroViewModel.getFavouriteHeroes().observe(viewLifecycleOwner) {
            allHeroAdapter.updateFavourites(it)
        }

        allHeroViewModel.searchResult.observe(viewLifecycleOwner) {
            allHeroAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        }
    }

    override fun onClick(hero: HeroResult) {
        val action =
            AllHeroesFragmentDirections.actionAllHeroFragmentToHeroDetailFragment(hero)
        findNavController().navigate(action)
    }

    override fun addToFavourite(hero: HeroResult) {
        allHeroViewModel.addToFavourite(hero)
    }

    override fun removeFromFavourite(hero: HeroResult) {
        allHeroViewModel.removeFromFavourite(hero)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.hero_menu, menu)

        val searchItem = menu.findItem(R.id.all_hero_search)
        val searchView = searchItem.actionView as SearchView

        searchView.queryHint = "Buscar héroe"
        searchView.setIconifiedByDefault(false)
        searchView.onActionViewExpanded()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    allHeroViewModel.searchQuery.value = newText
                }
                return true
            }
        })

    }
}