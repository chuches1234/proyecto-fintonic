package com.albertoalbaladejo.proyecto_marvel.ui.allheroes

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.albertoalbaladejo.proyecto_marvel.data.api.MarvelApi
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import retrofit2.HttpException

private const val STARTING_OFFSET = 0
private const val LOAD_SIZE = 20

class HeroPagingSource(
    private val marvelApi: MarvelApi,
    private val query: String
) : PagingSource<Int, HeroResult>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, HeroResult> {
        val position = (params.key ?: STARTING_OFFSET)
        return try {

            val heroes = if (query != "") {
                val response = marvelApi.searchHero(
                    query = query,
                    offset = position,
                    limit = params.loadSize
                )
                val data = response.data
                data.results
            } else {
                val response =
                    marvelApi.getAllHeros(offset = position, limit = params.loadSize)
                val data = response.data
                data.results
            }

            LoadResult.Page(
                data = heroes,
                prevKey = if (position == STARTING_OFFSET) null else position - LOAD_SIZE,
                nextKey = if (heroes.isEmpty()) null else position + LOAD_SIZE
            )
        } catch (exception: Exception) {
            Log.i("CHARACTER EXCEPTION", exception.toString())
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, HeroResult>): Int? {
        TODO("Not yet implemented")
    }
}