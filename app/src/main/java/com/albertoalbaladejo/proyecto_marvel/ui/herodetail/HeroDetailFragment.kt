package com.albertoalbaladejo.proyecto_marvel.ui.herodetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.albertoalbaladejocortes.proyecto_marvel.R
import com.albertoalbaladejocortes.proyecto_marvel.databinding.FragmentDetailHeroBinding
import com.albertoalbaladejo.proyecto_marvel.ui.herocomics.ComicFragment
import com.albertoalbaladejo.proyecto_marvel.ui.heroseries.SeriesFragment
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HeroDetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailHeroBinding
    private val navArgs by navArgs<HeroDetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailHeroBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val hero = navArgs.hero

        val fragments = arrayListOf(
            ComicFragment(hero.id),
            SeriesFragment(hero.id)
        )

        val viewPagerAdapter = ViewPagerAdapter(fragments, requireActivity())

        with(binding) {
            viewPager.adapter = viewPagerAdapter
            viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL

            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = when (position) {
                    0 -> "Comics"
                    1 -> "Series"
                    else -> "Comics"
                }
                tab.icon = when (position) {
                    0 -> ContextCompat.getDrawable(requireContext(), R.drawable.ic_book)
                    1 -> ContextCompat.getDrawable(requireContext(), R.drawable.ic_film)
                    else -> ContextCompat.getDrawable(requireContext(), R.drawable.ic_book)
                }
            }.attach()
        }
    }

}