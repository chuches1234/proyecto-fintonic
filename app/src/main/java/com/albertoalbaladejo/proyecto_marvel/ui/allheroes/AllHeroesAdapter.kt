package com.albertoalbaladejo.proyecto_marvel.ui.allheroes

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import com.albertoalbaladejo.proyecto_marvel.utils.Constants
import com.albertoalbaladejo.proyecto_marvel.utils.Mapper.HERO_MAPPER
import com.albertoalbaladejocortes.proyecto_marvel.R
import com.albertoalbaladejocortes.proyecto_marvel.databinding.ItemHeroBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

class AllHeroAdapter(private val listener: HeroClickListener) :
    PagingDataAdapter<HeroResult, AllHeroAdapter.AllHeroViewHolder>(HERO_MAPPER) {

    var favourites: List<HeroResult> = emptyList()

    inner class AllHeroViewHolder(private val binding: ItemHeroBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.heroCardView.animation =
                AnimationUtils.loadAnimation(binding.heroCardView.context, R.anim.scale)
            binding.heroCardView.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val hero = getItem(position)
                    if (hero != null) {
                        listener.onClick(hero)
                    }
                }
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(hero: HeroResult) {
            binding.apply {
                Glide.with(itemView)
                    .load(hero.thumbnail.path + "." + hero.thumbnail.extension)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            heroProgressbar.isVisible = false
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            heroProgressbar.isVisible = false
                            return false
                        }
                    })
                    .centerCrop().into(heroImage)
                heroName.text = hero.name
                heroDescription.text = hero.description.ifEmpty {
                    Constants.HeroConstant.NO_DESCRIPTION_AVAILABLE
                }
                heroComics.text =
                    "${Constants.HeroConstant.COMICS} ${hero.comics.available}"
                heroSeries.text =
                    "${Constants.HeroConstant.SERIES} ${hero.series.available}"
                likeButton.setImageResource(
                    if (favourites.any { it.id == hero.id }) R.drawable.ic_favourite_red
                    else R.drawable.ic_favourite
                )

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllHeroViewHolder {
        val binding =
            ItemHeroBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val allHeroViewHolder = AllHeroViewHolder(binding)
        Log.i("Fav item size: ", favourites.size.toString())
        binding.likeButton.setOnClickListener {
            val position = allHeroViewHolder.absoluteAdapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val hero = getItem(position)
                if (hero != null) {
                    val favHero = favourites.find { it.id == hero.id }
                    // Remove and add item from favourite
                    if (favHero != null) {
                        listener.removeFromFavourite(hero)
                    } else {
                        listener.addToFavourite(hero)
                    }
                }
            }
        }
        return allHeroViewHolder
    }

    override fun onBindViewHolder(holder: AllHeroViewHolder, position: Int) {
        val currentHero = getItem(position)
        if (currentHero != null) {
            holder.bind(currentHero)
        }
    }

    fun updateFavourites(favouritesUpdated: List<HeroResult>) {
        favourites = favouritesUpdated
        notifyDataSetChanged()
    }

}

interface HeroClickListener {
    fun onClick(hero: HeroResult)
    fun addToFavourite(hero: HeroResult)
    fun removeFromFavourite(hero: HeroResult)
}