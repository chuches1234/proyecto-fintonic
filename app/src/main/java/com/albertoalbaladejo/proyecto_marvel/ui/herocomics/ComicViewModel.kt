package com.albertoalbaladejo.proyecto_marvel.ui.herocomics

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.albertoalbaladejo.proyecto_marvel.data.api.repository.MarvelRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ComicViewModel @Inject constructor(private val marvelRepository: MarvelRepository) :
    ViewModel() {

    fun getHeroComics(heroId: String) =
        marvelRepository.getHeroComics(heroId).asLiveData().cachedIn(viewModelScope)

}