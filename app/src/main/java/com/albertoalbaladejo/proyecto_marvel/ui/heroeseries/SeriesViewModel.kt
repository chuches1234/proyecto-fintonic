package com.albertoalbaladejo.proyecto_marvel.ui.heroseries

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.albertoalbaladejo.proyecto_marvel.data.api.repository.MarvelRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SeriesViewModel @Inject constructor(private val marvelRepository: MarvelRepository) :
    ViewModel() {

    fun getHeroSeries(heroId: String) =
        marvelRepository.getHeroSeries(heroId).asLiveData().cachedIn(viewModelScope)

}