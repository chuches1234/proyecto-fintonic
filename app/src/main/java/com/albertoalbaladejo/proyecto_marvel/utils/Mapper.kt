package com.albertoalbaladejo.proyecto_marvel.utils

import androidx.recyclerview.widget.DiffUtil
import com.albertoalbaladejo.proyecto_marvel.data.models.comic.ComicResult
import com.albertoalbaladejo.proyecto_marvel.data.models.heroes.HeroResult
import com.albertoalbaladejo.proyecto_marvel.data.models.series.SeriesResult

object Mapper {

    val HERO_MAPPER = object : DiffUtil.ItemCallback<HeroResult>() {
        override fun areItemsTheSame(oldItem: HeroResult, newItem: HeroResult): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: HeroResult,
            newItem: HeroResult
        ): Boolean {
            return oldItem.name == newItem.name
        }
    }

    val HERO_COMICS_MAPPER = object : DiffUtil.ItemCallback<ComicResult>() {
        override fun areItemsTheSame(oldItem: ComicResult, newItem: ComicResult): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ComicResult, newItem: ComicResult): Boolean {
            return oldItem.title == newItem.title
        }
    }

    val HERO_SERIES_MAPPER = object : DiffUtil.ItemCallback<SeriesResult>() {
        override fun areItemsTheSame(oldItem: SeriesResult, newItem: SeriesResult): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: SeriesResult, newItem: SeriesResult): Boolean {
            return oldItem.title == newItem.title
        }
    }

}