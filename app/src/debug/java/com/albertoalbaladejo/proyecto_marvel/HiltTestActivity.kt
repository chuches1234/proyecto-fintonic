package com.albertoalbaladejo.proyecto_marvel

import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HiltTestActivity :AppCompatActivity()