package com.albertoalbaladejocortes.heroinfo.heroinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.albertoalbaladejocortes.heroinfo.R
import com.albertoalbaladejocortes.heroinfo.databinding.FragmentInfoHeroBinding
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InfoHeroFragment : Fragment() {

    private lateinit var binding: FragmentInfoHeroBinding

    private val navArgs by navArgs<InfoHeroFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInfoHeroBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = activity as AppCompatActivity
        //val view = requireActivity().findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        //view.visibility = View.GONE
        binding.toolbar.navigationIcon =
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_back)
        binding.toolbar.setNavigationOnClickListener {
            mainActivity.onBackPressed()
            view.visibility = View.VISIBLE
        }

        setView()
    }

    private fun setView() {
        with(binding) {
            Glide.with(requireContext())
                .load(navArgs.image + "." + navArgs.imageExtension).into(heroImage)
            heroTitle.text = navArgs.title ?: resources.getText(R.string.no_result_found)
            heroDescription.text = navArgs.description ?: resources.getText(R.string.no_result_found)
        }
    }

}